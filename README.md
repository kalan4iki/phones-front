 [![pipeline status](https://gitlab.com/kalan4iki/phones-front/badges/master/pipeline.svg)](https://gitlab.com/kalan4iki/phones-front/-/commits/master) 

# phone

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
