import Vue from 'vue'
import VueRouter from 'vue-router'
import ipphone from '../components/ipphone.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: ipphone
  },
  {
    path: '*',
    redirect: '/'
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
